import Image from "next/image";
import PropTypes from "prop-types";
import { useState } from "react";
import { Button, Form, FormGroup, Input, Label } from "reactstrap";

export default function BlogForm({ handleSubmit, isLoading, titleValue, descValue, imgUrl }) {
  const [title, setTitle] = useState(titleValue);
  const [description, setDescription] = useState(descValue);
  const [file, setFile] = useState(imgUrl);

  return (
    <Form>
      <FormGroup>
        <Label for="title">Title</Label>
        <Input id="title" name="title" placeholder="title" type="text" required maxLength={50} value={title} onChange={(e) => setTitle(e.target.value)} />
      </FormGroup>
      <FormGroup>
        <Label for="description">Body</Label>
        <Input id="description" name="description" type="textarea" maxLength={500} value={description} onChange={(e) => setDescription(e.target.value)} />
      </FormGroup>
      <FormGroup>
        <Label for="exampleFile">File</Label>
        <Input id="exampleFile" name="file" type="file" onChange={(e) => setFile(e.target.files[0])} />
        {file !== "" && (
          <div className={"image_preview"}>
            <Image src={URL.createObjectURL(file)} alt="Sample" fill className={"image"} />
          </div>
        )}
      </FormGroup>
      <Button
        onClick={() => {
          setDescription("");
          setTitle("");
          handleSubmit({ title, description, file });
        }}
        disabled={isLoading}
      >
        Submit
      </Button>
    </Form>
  );
}

BlogForm.propTypes = {
  imgUrl: PropTypes.string,
  titleValue: PropTypes.string,
  descValue: PropTypes.string,
  handleSubmit: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
};
BlogForm.defaultProps = {
  titleValue: "",
  descValue: "",
  imgUrl: "",
};
