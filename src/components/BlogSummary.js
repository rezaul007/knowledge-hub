const {
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardText,
  Button,
} = require("reactstrap");
import Image from "next/image";
import { useRouter } from "next/router";

export const BlogSummary = ({data}) => {
   const router = useRouter();
  const handleDetailsClick = ()=>{
    router.push(`blog/${data.id}`)
  }
  return (
    <Card className="borderless-card">
      <div className={"image_container"}>
        <Image
          src="https://picsum.photos/300/200"
          alt="Sample"
          fill
          className={"image"}
        />
      </div>
      <CardBody>
        <CardTitle tag="h5">{data?.title}</CardTitle>
        <CardSubtitle className="mb-2 text-muted" tag="h6">
          Published: 02 Mar 2023, 21: 04
        </CardSubtitle>
        <CardText>
          {data?.body.slice(0,50)+"..."}
        </CardText>
        <Button outline onClick={handleDetailsClick}>Details</Button>
      </CardBody>
    </Card>
  );
};
