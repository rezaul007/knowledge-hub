import { Container } from "reactstrap";
import Header from "./header";
const Layout = ({ children }) => {
  return (
    <Container fluid>
      <Header />
      {children}
    </Container>
  );
};
export default Layout;
