import { signOut, useSession } from "next-auth/react";
import { useState } from "react";
import { Button, Collapse, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem, NavLink, Navbar, NavbarBrand, NavbarToggler, UncontrolledDropdown } from "reactstrap";

const Header = (args) => {
  const { data: session, status } = useSession();
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  return (
    <Navbar {...args} dark color="dark" className="mb-4" expand={"lg"}>
      <NavbarBrand href="/">Knowledge Hub</NavbarBrand>
      <NavbarToggler onClick={toggle} />
      <Collapse isOpen={isOpen} navbar>
        <Nav className="me-auto" navbar>
          {status === "authenticated" && (
            <>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  My Posts
                </DropdownToggle>
                <DropdownMenu end>
                  <DropdownItem>
                    <NavLink href="/dashboard/blog/create" className="text-dark">
                      New Post
                    </NavLink>
                  </DropdownItem>
                  <DropdownItem>
                    <NavLink href="/dashboard/blog/" className="text-dark">
                      List
                    </NavLink>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
              {session.isAdmin && (
                <NavItem>
                  <NavLink href="/dashboard/users/">Users</NavLink>
                </NavItem>
              )}
            </>
          )}
        </Nav>
        <Nav navbar>
          {status === "unauthenticated" && (
            <>
              <NavItem>
                <NavLink href="/auth/login/">Login</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/auth/registration/">Registration</NavLink>
              </NavItem>
            </>
          )}
          {status === "authenticated" && (
            <>
              <NavItem className="ml-4">
                <span className="text-white">{session.name}</span>
              </NavItem>
              <NavItem>
                <Button size="sm" onClick={signOut}>
                  Logout
                </Button>
              </NavItem>
            </>
          )}
        </Nav>
      </Collapse>
    </Navbar>
  );
};

export default Header;
