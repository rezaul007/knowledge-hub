import useSWR from "swr";

const fetcher = (url) => fetch(url).then((res) => res.json());

export const useGetBlogs = () => {
  const { data, error, isLoading } = useSWR("/api/blog/", fetcher);

  return { data, error, isLoading };
};

export const useMyBlogs = (offset, limit) => {
  const { data, error, isLoading } = useSWR(`/api/blog/my-blog/?offset=${offset}&limit=${limit}`, fetcher);
  return { data, error, isLoading };
};

export const useGetUsers = () => {
  const { data, error, isLoading } = useSWR("/api/user/", fetcher);

  return { data, error, isLoading };
};
