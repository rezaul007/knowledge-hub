const multer = require("multer");
const path = require("path");

const UPLOADS_FOLDER = "./public/images/";

const handler = {};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, UPLOADS_FOLDER);
  },
  filename: (req, file, cb) => {
    const exisTion = path.extname(file.originalname);
    req.fileName = file.originalname.replace(exisTion, "").toLowerCase().split(" ").join("-") + "-" + Date.now() + exisTion;
    cb(null, req.fileName);
  },
});

handler.upload = multer({
  storage: storage,
  limits: 100000,
  fileFilter: (req, file, cb) => {
    if (file.mimetype === "image/png" || file.mimetype === "image/jpg" || file.mimetype === "image/jpeg") {
      cb(null, true);
    } else {
      cb(new Error("profile should be .png, .jpg and .jpeg"));
    }
  },
});

handler.runMiddleware = (req, res, fn) => {
  return new Promise((resolve, reject) => {
    fn(req, res, (result) => {
      if (result instanceof Error) {
        return reject(result);
      }
      return resolve(result);
    });
  });
};

export default handler;
