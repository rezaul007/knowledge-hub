import Image from "next/image";
import { Col, Row } from "reactstrap";
import style from "./blogdetails.module.css";

const BlogDetails = ({ data }) => {
  console.log("data", data);
  return (
    <div className={style.blogDetails_root}>
      <Row>
        <Col md={6} sm={12} className="offset-md-3 mb-4">
          <div className={"image_container"}>
            <Image src="https://picsum.photos/300/200" alt="Sample" fill className={"image"} />
          </div>
        </Col>
        <h1>{data?.title}</h1>
        <Col sm={12}>{data?.body}</Col>
      </Row>
    </div>
  );
};
export default BlogDetails;

export const getServerSideProps = async (context) => {
  const blogId = context.params?.blogId;
  const data = await fetch(`https://jsonplaceholder.typicode.com/posts/${blogId}`, {
    method: "GET",
  });
  const jsonData = await data.json();
  return {
    props: { data: jsonData },
  };
};
