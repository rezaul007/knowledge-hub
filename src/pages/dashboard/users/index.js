import { useGetUsers } from "@/helper/blogs";
import Head from "next/head";
import { useRouter } from "next/router";
import DataTable from "react-data-table-component";
import { Button, Container } from "reactstrap";
import { useSWRConfig } from "swr";

export default function UserList() {
  const { data, error, isLoading } = useGetUsers();
  const { mutate } = useSWRConfig();
  const router = useRouter();
  const deleteHandler = async (id) => {
    try {
      await fetch(`/api/user/${id}/`, {
        method: "DELETE",
      });
      mutate("/api/user/");
    } catch (err) {
      console.log(err);
    }
  };

  const columns = [
    {
      name: "Name",
      selector: (row) => row.name,
    },
    {
      name: "Email",
      selector: (row) => row.email,
    },
    {
      name: "Actions",
      cell: (row) => (
        <>
          <Button onClick={() => deleteHandler(row._id)} color="danger" size="sm">
            Delete
          </Button>
        </>
      ),
    },
  ];

  if (error) return <div>Failed to load</div>;

  return (
    <>
      <Head>
        <title>Knowledge Hub</title>
        <meta name="description" content="gather knowledge here" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Container>
        <h2 className="text-center mb-5">User List</h2>
        <DataTable columns={columns} data={data?.result || []} progressPending={isLoading} />
      </Container>
    </>
  );
}
