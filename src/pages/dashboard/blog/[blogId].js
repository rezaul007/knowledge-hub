import BlogForm from "@/components/BlogForm";
import dbConnect from "@/helper/db";
import Blogs from "@/models/blogsModel";
import Head from "next/head";
import { useRouter } from "next/router";
import { useState } from "react";
import { Col, Container, Row } from "reactstrap";
import { useSWRConfig } from "swr";

export default function UpdateBlog({ data, error }) {
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();
  const { mutate } = useSWRConfig();

  if (error) return <div className="text-danger">No data found</div>;
  const handleSubmit = async (values) => {
    try {
      setIsLoading(true);
      await fetch(`/api/blog/${data?._id}/`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(values),
      });
      setIsLoading(false);
      mutate("/api/blog/");
      router.push("/dashboard/blog/");
    } catch (err) {
      console.log(err);
      setIsLoading(false);
    }
  };
  return (
    <>
      <Head>
        <title>Knowledge Hub</title>
        <meta name="description" content="gather knowledge here" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Container>
        <Row>
          <Col sm={12} md={8} className="offset-md-2">
            <h2 className="text-center mb-5">Post New Knowledge</h2>
            <BlogForm isLoading={isLoading} handleSubmit={handleSubmit} titleValue={data?.title} descValue={data?.description} />
          </Col>
        </Row>
      </Container>
    </>
  );
}

export const getServerSideProps = async (context) => {
  try {
    const blogId = context.params?.blogId;
    await dbConnect();
    const blog = await Blogs.findById(blogId).exec();
    return {
      props: {
        data: JSON.parse(JSON.stringify(blog)),
      },
    };
  } catch (error) {
    return {
      props: {
        error: JSON.stringify(error),
      },
    };
  }
};
