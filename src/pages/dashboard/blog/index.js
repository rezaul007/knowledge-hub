import { useMyBlogs } from "@/helper/blogs";
import Head from "next/head";
import { useRouter } from "next/router";
import { useState } from "react";
import DataTable from "react-data-table-component";
import { Button, Container } from "reactstrap";
import { useSWRConfig } from "swr";

export default function BlogList() {
  const { mutate } = useSWRConfig();
  const [limit, setLimit] = useState(10);
  const [offset, setOffset] = useState(10);
  const router = useRouter();
  const { data, error, isLoading } = useMyBlogs(offset, limit);

  const calculateOffset = (oLimit, oPage) => {
    let result = "0";
    if (oPage === 1) {
      return result;
    }
    result = oLimit * oPage - oLimit;
    return result;
  };
  const onPageChange = (nPage) => {
    const cOffset = calculateOffset(limit, nPage);
    setOffset(cOffset);
  };
  const onChangeRowsPerPage = (nRowsPerpage, nPage) => {
    const cOffset = calculateOffset(nRowsPerpage, nPage);
    setOffset(cOffset);
    setLimit(nRowsPerpage);
  };
  const deleteHandler = async (id) => {
    try {
      await fetch(`/api/blog/${id}/`, {
        method: "DELETE",
      });
      mutate("/api/blog/my-blog/");
    } catch (err) {
      console.log(err);
    }
  };

  const columns = [
    {
      name: "Post Title",
      selector: (row) => row.title,
    },
    {
      name: "Actions",
      cell: (row) => (
        <>
          <Button onClick={() => router.push(`/dashboard/blog/${row._id}`)} size="sm" className="ml-3">
            Edit
          </Button>
          <Button onClick={() => deleteHandler(row._id)} color="danger" size="sm">
            Delete
          </Button>
        </>
      ),
    },
  ];

  if (error) return <div>Failed to load</div>;

  return (
    <>
      <Head>
        <title>Knowledge Hub</title>
        <meta name="description" content="gather knowledge here" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Container>
        <h2 className="text-center mb-5">My Posts</h2>
        <DataTable
          columns={columns}
          data={data?.result || []}
          progressPending={isLoading}
          pagination
          paginationServer
          onChangePage={onPageChange}
          onChangeRowsPerPage={onChangeRowsPerPage}
          paginationPerPage={limit}
          paginationTotalRows={data?.count || 0}
        />
      </Container>
    </>
  );
}
