import BlogForm from "@/components/BlogForm";
import Head from "next/head";
import { useRouter } from "next/router";
import { useState } from "react";
import { Col, Container, Row } from "reactstrap";

export default function CreateBlog() {
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();

  const handleSubmit = async (values) => {
    const fromData = new FormData();
    fromData.append("files", values.file);
    fromData.append("title", values.title);
    fromData.append("description", values.description);

    try {
      setIsLoading(true);
      await fetch("/api/blog", {
        method: "POST",
        // headers: {
        //   "Content-Type": "multipart/form-data",
        // },
        body: fromData, //JSON.stringify(values),
      });
      setIsLoading(false);
      router.push("/dashboard/blog/");
    } catch (err) {
      console.log(err);
      setIsLoading(false);
    }
  };
  return (
    <>
      <Head>
        <title>Knowledge Hub</title>
        <meta name="description" content="gather knowledge here" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Container>
        <Row>
          <Col sm={12} md={8} className="offset-md-2">
            <h2 className="text-center mb-5">Post New Knowledge</h2>
            <BlogForm isLoading={isLoading} handleSubmit={handleSubmit} />
          </Col>
        </Row>
      </Container>
    </>
  );
}
