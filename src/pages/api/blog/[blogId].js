import dbConnect from "@/helper/db";
import Blogs from "../../../models/blogsModel";

export default async function handler(req, res) {
  const { blogId } = req.query;
  if (req.method === "DELETE") {
    try {
      await dbConnect();
      await Blogs.deleteOne({ _id: blogId });
      res.status(200).json({ message: "Deleted Successfully" });
    } catch (error) {
      res.status(400).json({ error });
    }
  }
  if (req.method === "PUT") {
    try {
      await dbConnect();
      await Blogs.updateOne({ _id: blogId }, req.body);
      res.status(200).json({ message: "Updated Successfully" });
    } catch (error) {
      res.status(400).json({ error });
    }
  }
}
