import { getToken } from "next-auth/jwt";
import { createRouter } from "next-connect";
import dbConnect from "../../../helper/db";
import handler from "../../../helper/uploadFile";
import Blogs from "../../../models/blogsModel";

const router = createRouter();
router
  .post(async (req, res) => {
    try {
      const token = await getToken({ req });
      if (!token) {
        res.status(401).json({ message: "Unauthorized!" });
      }
      const fileUpload = handler.upload.single("files");
      await handler.runMiddleware(req, res, fileUpload);
      await dbConnect();
      const blogs = await Blogs.create({ ...req.body, user: token.id, img: `http://${req.headers.host}/images/${req.fileName}` });
      res.status(200).json({ data: blogs });
    } catch (error) {
      console.log("error -> : ", error);
      res.status(400).json({ error });
    }
  })
  .get(async (req, res) => {
    try {
      await dbConnect();
      const blogs = await Blogs.find();
      res.status(200).json({ result: blogs });
    } catch (error) {
      res.status(400).json({ error });
    }
  });

export const config = {
  api: {
    bodyParser: false,
  },
};

export default router.handler({
  onError(err, req, res) {
    res.status(500).json({
      error: err.message,
    });
  },
});
