import dbConnect from "@/helper/db";
import Blogs from "@/models/blogsModel";
import { getToken } from "next-auth/jwt";

export default async function handler(req, res) {
  const token = await getToken({ req });
  if (!token) {
    res.status(401).json({ message: "Unauthorized!" });
  }
  if (req.method !== "GET") {
    res.status(405).json({ message: "Method not allowed!" });
  }
  try {
    const { offset, limit } = req.query;
    await dbConnect();
    const blogs = await Blogs.find({ user: token?.id }).sort({ createdAt: -1 }).skip(offset).limit(limit);
    const totalCount = await Blogs.countDocuments({ user: token?.id });
    res.status(200).json({ result: blogs, count: totalCount });
  } catch (error) {
    console.log("error", error);
    res.status(400).json({ error });
  }
}
