import dbConnect from "@/helper/db";
import Users from "../../../models/userModel";

export default async function handler(req, res) {
  const { userId } = req.query;
  if (req.method === "DELETE") {
    try {
      await dbConnect();
      await Users.deleteOne({ _id: userId });
      res.status(200).json({ message: "Deleted Successfully" });
    } catch (error) {
      res.status(400).json({ error });
    }
  }
}
