import dbConnect from "../../../helper/db";
import Users from "../../../models/userModel";

export default async function handler(req, res) {
  if (req.method === "GET") {
    try {
      await dbConnect();
      const users = await Users.find();
      res.status(200).json({ result: users });
    } catch (error) {
      res.status(400).json({ error });
    }
  }
}
