import { verifyPassword } from "@/helper/auth";
import dbConnect from "@/helper/db";
import Users from "@/models/userModel";
import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";

export default NextAuth({
  session: {
    strategy: "jwt",
  },
  providers: [
    CredentialsProvider({
      async authorize(credentials) {
        if (!credentials.email || !credentials.password) {
          throw new Error("credentials required!");
        }

        await dbConnect();
        const user = await Users.findOne({ email: credentials.email }).exec();
        if (!user) {
          throw new Error("No user found!");
        }

        if (user.isBlock) {
          throw new Error("Can not Login!");
        }
        const isValid = await verifyPassword(credentials.password, user.password);

        if (!isValid) {
          throw new Error("Credential Incorrect!");
        }
        return user;
      },
    }),
  ],
  callbacks: {
    jwt: ({ token, user }) => {
      if (user) {
        token.id = user._id.toString();
        token.name = user.name;
        token.email = user.email;
        token.isAdmin = user.isAdmin;
      }
      return Promise.resolve(token);
    },
    session: ({ session, token }) => {
      session.id = token.id;
      session.name = token.name;
      session.email = token.email;
      session.isAdmin = token.isAdmin;
      return Promise.resolve(session);
    },
  },
  pages: {
    signIn: "/auth/login",
  },
});
