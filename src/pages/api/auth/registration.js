var bcrypt = require("bcryptjs");
import { hashPassword } from "@/helper/auth";
import dbConnect from "../../../helper/db";
import Users from "../../../models/userModel";

export default async function handler(req, res) {
  if (req.method === "POST") {
    try {
      const hash = await hashPassword(req.body.password);
      await dbConnect();
      const users = await Users.create({ ...req.body, password: hash });
      res.status(200).json({ users, message: "registration successful" });
    } catch (error) {
      res.status(400).json({ error });
    }
  }
}
