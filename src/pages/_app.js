import Layout from "@/components/layout";
import "bootstrap/dist/css/bootstrap.css";
import { SessionProvider } from "next-auth/react";
import "../styles/globar.css";

export default function App({ Component, pageProps }) {
  return (
    <SessionProvider session={pageProps.session}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </SessionProvider>
  );
}
