import { BlogSummary } from "@/components/BlogSummary";
import Head from "next/head";
import { Col, Container, Row } from "reactstrap";

export default function Home(props) {
  const { data } = props;
  return (
    <>
      <Head>
        <title>Knowledge Hub</title>
        <meta name="description" content="gather knowledge here" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Container>
        <Row>
          {data?.map((blog) => (
            <Col sm={12} md={4} key={blog.id}>
              <BlogSummary data={blog} />
            </Col>
          ))}
        </Row>
      </Container>
    </>
  );
}

export const getStaticProps = async () => {
  const data = await fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "GET",
  });
  const jsonData = await data.json();
  return {
    props: { data: jsonData },
    revalidate: 60,
  };
};
