const { Form, FormGroup, Label, Col, Input, FormFeedback, Button, Card, CardTitle, CardBody, CardHeader, Row } = require("reactstrap");
import { useFormik } from "formik";
import { signIn } from "next-auth/react";
import { useRouter } from "next/router";
import * as Yup from "yup";

const Login = () => {
  const router = useRouter();
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: Yup.object().shape({
      email: Yup.string().required("Email Required"),
      password: Yup.string().required("Password Required"),
    }),
    onSubmit: async (values) => {
      try {
        const res = await signIn("credentials", {
          ...values,
          redirect: false,
        });
        if (res.ok && !res.error) {
          router.push("/dashboard/blog/");
        } else {
          console.log("res", res);
        }
      } catch (err) {
        console.log("err", err);
      }
    },
  });

  return (
    <Row>
      <Col sm={12} md={6} className="offset-md-3">
        <Card>
          <CardHeader>Login</CardHeader>
          <CardBody>
            <Form onSubmit={formik.handleSubmit}>
              <FormGroup row>
                <Label for="email" sm={12}>
                  Email
                </Label>
                <Col sm={12}>
                  <Input
                    autoComplete=""
                    id="email"
                    name="email"
                    placeholder="enter email"
                    type="email"
                    invalid={formik.touched.email && formik.errors.email}
                    onChange={formik.handleChange}
                    value={formik.values.email}
                  />
                  <FormFeedback>{formik.errors.email}</FormFeedback>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="password" sm={12}>
                  Password
                </Label>
                <Col sm={12}>
                  <Input
                    id="password"
                    name="password"
                    placeholder="enter password"
                    type="password"
                    invalid={formik.touched.password && formik.errors.password}
                    onChange={formik.handleChange}
                    value={formik.values.password}
                  />
                  <FormFeedback>{formik.errors.password}</FormFeedback>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col>
                  <Button type="submit">Submit</Button>
                </Col>
              </FormGroup>
            </Form>
          </CardBody>
        </Card>
      </Col>
    </Row>
  );
};

export default Login;

// export const getServerSideProps = async () => {
//   const session = await getSession();
//   if (session) {
//     return {
//       redirect: {
//         destination: "/ dashboard/blog",
//       },
//     };
//   } else {
//     return { props: {} };
//   }
// };
