const { Form, FormGroup, Label, Col, Input, FormFeedback, Button, Card, CardBody, CardHeader, Row } = require("reactstrap");
import { useFormik } from "formik";
import { useRouter } from "next/router";
import { useState } from "react";
import * as Yup from "yup";

const Registration = () => {
  const [loading, setIsLoading] = useState();
  const router = useRouter();
  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      password: "",
      confirmPassword: "",
    },
    validationSchema: Yup.object().shape({
      name: Yup.string().required("Username Required"),
      email: Yup.string().email("Invalid email format").required("Email required"),
      password: Yup.string().required("Password Required"),
      confirmPassword: Yup.string().oneOf([Yup.ref("password")], "Passwords must match"),
    }),
    onSubmit: async (values, { resetForm }) => {
      try {
        const payload = { ...values };
        delete payload?.confirmPassword;
        setIsLoading(true);
        await fetch("/api/auth/registration/", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(payload),
        });
        setIsLoading(false);
        resetForm();
        router.push("/auth/login/");
      } catch (err) {
        console.log(err);
        setIsLoading(false);
      }
    },
  });
  return (
    <Row>
      <Col sm={12} md={6} className="offset-md-3">
        <Card>
          <CardHeader>Registration</CardHeader>
          <CardBody>
            <Form onSubmit={formik.handleSubmit}>
              <FormGroup row>
                <Label for="name" sm={2}>
                  Name
                </Label>
                <Col sm={12}>
                  <Input
                    autoComplete=""
                    id="name"
                    name="name"
                    placeholder="Enter name"
                    type="text"
                    invalid={formik.touched.Name && formik.errors.name}
                    onChange={formik.handleChange}
                    value={formik.values.name}
                  />
                  <FormFeedback>{formik.errors.name}</FormFeedback>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="email" sm={2}>
                  Email
                </Label>
                <Col sm={12}>
                  <Input
                    autoComplete=""
                    id="email"
                    name="email"
                    placeholder="Enter email"
                    type="text"
                    invalid={formik.touched.email && formik.errors.email}
                    onChange={formik.handleChange}
                    value={formik.values.email}
                  />
                  <FormFeedback>{formik.errors.email}</FormFeedback>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="password" sm={2}>
                  Password
                </Label>
                <Col sm={12}>
                  <Input
                    id="password"
                    name="password"
                    placeholder="Enter password"
                    type="password"
                    invalid={formik.touched.password && formik.errors.password}
                    onChange={formik.handleChange}
                    value={formik.values.password}
                  />
                  <FormFeedback>{formik.errors.password}</FormFeedback>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="confirmPassword" sm={3}>
                  Confirm Password
                </Label>
                <Col sm={12}>
                  <Input
                    id="confirmPassword"
                    name="confirmPassword"
                    placeholder="Enter confirm password"
                    type="password"
                    invalid={formik.touched.confirmPassword && formik.errors.confirmPassword}
                    onChange={formik.handleChange}
                    value={formik.values.confirmPassword}
                  />
                  <FormFeedback>{formik.errors.confirmPassword}</FormFeedback>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col>
                  <Button type="submit" disabled={loading}>
                    Submit
                  </Button>
                </Col>
              </FormGroup>
            </Form>
          </CardBody>
        </Card>
      </Col>
    </Row>
  );
};

export default Registration;

// export const getServerSideProps = async () => {
//   const session = await getSession();
//   if (session) {
//     return {
//       redirect: {
//         destination: "/ dashboard/blog",
//       },
//     };
//   }
// };
